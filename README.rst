Del 0 - Git
===========

**NB: Hvis du ikke har satt opp en GitLab konto:** gå tilbake til tutorialen
gitt i https://git.app.uib.no/inf100-students-v2020/technical-setup-tutorial

Denne delen vil være tilstede på hver øving.
For å komme i gang med prosjektet ditt, må du gjøre disse fire tingene vi skal
beskreve i mer detalj nedenfor:

1. **Sjekk** at du fikk din egen kopi av prosjektet *på git*. **Forking trenges ikke lenger!**
2. **Clone** - Få din versjon av prosjektet inn i VSCode *på din lokale datamaskin*.
3. **Commit** - Lagre dine endringer lokalt.
4. **Push** - Laste opp dine endringer til ditt git prosjekt.

Det er viktig å gjøre alle disse stegene, den vanligste/kjipeste feilen er å
glemme å pushe (som er det samme som å glemme å levere!). **Unngå helst dette**.

Sjekk
#####
Først skal du sjekke at du fikk din egen *fork* av prosjektet.
For å gjøre dette, følg disse stegene:

1. Gå til gruppesiden https://git.app.uib.no/inf100-assignments-v2020/
2. Klikk på oppgave-5-prosjektet med navnet ditt
3. Sjekk at URL'en ser slik ut:
   ``https://git.app.uib.no/inf100-assignments-v2020/<dittbrukernavn>_inf100-v2020-oppgave-5``,
   hvor ``<dittbrukernavn>`` er ditt git.app.uib.no navn.

Det er viktig at dere ikke endrer på navn til prosjekt / filer, med mindre
det står at dere skal gjøre det.

Cloning
#######

Nå har du laget din egen versjon av prosjektet *på git*. Neste steg er å
få prosjektet lastet inn i VSCode på din datamaskin.

1. Oppe til høyre i git.app.uib.no, klikk på **Clone**, og kopier SSH-linken
   som står under **Clone with SSH**.
2. Gå inn i VSCode, og åpne opp en mappe du bruker til INF100.
3. Mens du er i mappen, åpne opp terminalen og skriv ``git clone copypasteURL``,
   hvor ``copypasteURL`` byttes ut med URL'en fra steg 1.
4. Nå skal du ha en lokal versjon av repo'et på din maskin.
5. I VSCode, klikk ``File>Open`` (``File>Open Folder`` på windows). Åpne
   mappen ``inf100-v2020-oppgave-5``.
6. Nå kan du gjøre oppgavene og kjøre koden lokalt.

Commit og Push
##############
**PS/NB/VIKTIG:** Når du har gjort en eller flere oppgaver, **husk å commit OG PUSH**
for å få endringene dine opp på git.

**Commit**:
Her "lagrer" du endringene du har gjort **lokalt på din maskin**. For å lage en
commit/lagring, velger du noen filer (f.eks ``del_1.py``), og oppgir en
melding (f.eks ``Finished del 1!``, eller ``Fixed bug in del 1``).

1. Åpn opp git-vinduet ved å klikke på Source Control ikonet til venstre i VSCode
   (under forstørrelsesglasset).
2. For å lage **en** commit, velg noen filer, trykk på pluss, skriv en melding,
   og trykk på haken.

**Push**:
Nå har du commitet, MEN de eksisterer kun på **den maskinen du bruker, og IKKE på git**.
For å få endringene lastet opp til git.app.uib serveren, må du *pushe*.
For å pushe, skriv ``git push`` i terminalen. Sjekk så på git.app.uib.no at
endringene dine er lastet opp. For å se alle commits som er pushet opp, kan du
gå på ``Repository>Graph`` (se f.eks https://git.app.uib.no/inf100-students-v2020/technical-setup-tutorial/-/network/master )


Del 1 - Numpy basics
====================

Fullfør funksjonene definert i ``del_1.py``.

Del 2 - Nyttige funksjoner for del 3 / Repetisjon
=================================================

Fullfør funksjonene definert i ``del_2.py``. De kan brukes i del 3.


Del 3 - Problemløsing og resonnering
====================================

I filen ``sequences.fasta`` finnes 86 sekvenser av koronaviruset.
(`Kilde: NCBI virus, Download <https://www.ncbi.nlm.nih.gov/labs/virus/vssi/#/virus?SeqType_s=Nucleotide&VirusLineage_ss=SARS-CoV-2,%20taxid:2697049>`_)
Filen er i FASTA format, som ligner formatet vi så i ``example.txt`` fra del 2.

Oppgaven er å sammenligne alle sekvenser med hverandre. Vi skal telle opp hvor
stor forskjellen i sekvensen er i hvert par. Denne informasjonen kan f.eks brukes
for å lage diagrammene vi finner på `nextstrain.org <https://nextstrain.org/ncov/>`_

Kommentarene i ``del_3.py`` forklarer
løsningsveien. Flere funksjoner er allerede ferdig, du skal fylle ut resten. Les
hele koden først før du begynner!


Just so...
==========
For en spennende bruk av Python and Javascript i data-analyse, se på

* https://nextstrain.org/narratives/ncov/sit-rep/2020-03-20
* https://nextstrain.org/ncov/
* https://nextstrain.org/narratives/trees-background/
* https://github.com/nextstrain/ncov
