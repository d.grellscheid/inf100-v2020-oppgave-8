import numpy as np

def a(N):
    # Return a numpy array of N values of 0.0
    pass

def b(N, i):
    # Return a numpy array of N values of 0.0
    # except the value in ith position should be 1.0
    pass

def c(A,B):
    # Return a numpy array of length 50, with values evenly spread from A to B
    # The first value is A, the last value is B
    pass

def d(arr):
    # return the numpy array backwards, starting with the last element to the first
    pass

def e(M,N):
    # return a 2-dimensional numpy array of shape (M,N) with zeros in the middle and
    # ones on the edge:
    # 1 1 1 1 1 1 1 1
    # 1 0 0 0 0 0 0 1
    # 1 0 0 0 0 0 0 1
    # 1 1 1 1 1 1 1 1
    pass

def f(L,M,N):
    # return a 3-dimensional numpy array of shape (L,M,N) with zeros in the middle and
    # ones on the surface
    pass

def g(arr):
    # change the sign from + to - of all elements in arr that are between 10 and 20
    # return the new array
    pass
