import numpy as np

def load_file(filename):
    """
    Load a file formatted like example.txt and return two lists:
    a list of titles, and a list of joined texts.

    Each title is marked with '>' at the beginning of the line,
    keep the '>' mark as part of the title.

    The lines that follow should be joined together as one string, until the
    next title starts.
    """
    titles = []
    texts = []

    # din kode her

    return (titles, texts)


def str_as_array(text):
    """
    Returns a numpy array with one character from text in each element:

    >>> str_as_array('Hello')
    array(['H', 'e', 'l', 'l', 'o'])
    """

    # din kode her

    pass


def count_in_array(data, value):
    """
    Return a count of how often value exists in the data array.
    With numpy boolean arrays (filters) and numpy.nonzero this can be done without loops

    >>> count_in_array(np.array([7,3,9,2,1,6,4,3,8]), 3)
    2
    """

    # din kode her

    return 0


def count_differences(data1, data2):
    """
    Return a count of how many elements are different between data1 and data2
    as we go from left to right in each position.

    data1 and data2 have the same length, you don't need to handle different lengths.

    Using numpy filters and np.nonzero, it will look almost the same as
    count_in_array

    >>> count_differences(np.array([7,3,5]), np.array([7,3,5]))
    0

    >>> count_differences(np.array([7,5,3]), np.array([7,3,5]))
    2

    >>> count_differences(np.array([7,3,9,2,3,8,1]), np.array([7,3,5,2,3,8,1]))
    1
    """

    # din kode her

    return 0








if __name__ == "__main__":
    titles, texts = load_file('example.txt')

    if titles and len(titles) == len(texts):
        print('OK! Number fits.')
    else:
        print('Titles and texts should be same length')

    if titles[2] == "> Moby Dick":
        print('Titles OK!')

    if texts[0][:10] == 'Alice was ' and texts[0][-10:] == 'se by her.':
        print('Alice OK!')

    if str_as_array('Hello') == np.array(['H', 'e', 'l', 'l', 'o']):
        print('OK! str_as_array works')

    if count_in_array(np.array([7,3,9,2,1,6,4,3,8]), 3) == 2:
        print('OK! count_in_array works')

    if count_differences(np.array([7,3,9,2,3,6,1]), np.array([7,3,5,2,3,8,1])) == 2:
        print('OK! count_differences works')
