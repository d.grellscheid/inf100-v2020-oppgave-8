# Include folder above in python path
# Helps travis CI runner find the src files when running py.test
import pytest
import numpy as np

import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

## Tests go below here
import del_2 as M

def test_del_2_load_1():
    titles, texts = M.load_file('example.txt')
    assert len(titles) == len(texts) == 3

def test_del_2_load_2():
    titles, texts = M.load_file('example.txt')
    assert titles[2] == "> Moby Dick"

def test_del_2_load_3():
    titles, texts = M.load_file('example.txt')
    assert texts[0][:10] == 'Alice was ' and texts[0][-10:] == 'se by her.'

def test_del_2_load_4():
    titles, texts = M.load_file('tests/del2example.txt')
    assert len(titles) == len(texts) == 5

def test_del_2_load_5():
    titles, texts = M.load_file('tests/del2example.txt')
    assert titles[3] == "> Praesent2"

def test_del_2_load_6():
    titles, texts = M.load_file('tests/del2example.txt')
    assert texts[4][:10] == 'Praesent3 ' and texts[2][-10:] == 'rbi mattis'



def test_del_2_str_array():
    assert np.all(
       M.str_as_array('Hello there!')
          ==
       np.array(['H', 'e', 'l', 'l', 'o', ' ', 't', 'h', 'e', 'r', 'e', '!'])
    )

def test_del_2_count_array_1():
    assert M.count_in_array(np.array([7,3,9,2,3,1,6,4,3,8]), 3) == 3

def test_del_2_count_array_2():
    assert M.count_in_array(np.array([7,8,9]), 3) == 0

def test_del_2_count_diffs_1():
    assert M.count_differences(np.array([7,3,9,2,3,6,1]), np.array([7,3,5,2,3,8,1])) == 2

def test_del_2_count_diffs_2():
    assert M.count_differences(np.array([1,2,3]), np.array([1,3,2])) == 2

def test_del_2_count_diffs_3():
    assert M.count_differences(np.array([5,1,2]), np.array([5,1,2])) == 0
