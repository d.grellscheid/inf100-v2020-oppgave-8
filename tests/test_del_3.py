# Include folder above in python path
# Helps travis CI runner find the src files when running py.test
import pytest
import numpy as np

import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

## Tests go below here
import del_3 as M

def test_del_3_load_1():
    titles, seqs = M.load_data('sequences.fasta')
    assert len(titles) == len(seqs) == 86

def test_del_3_load_2():
    titles, seqs = M.load_data('sequences.fasta')
    assert 'MT188340' in titles[5]
    assert seqs[5][2075:2275] == 'AAAAACTCAAACCCGTCCTTGATTGGCTTGAAGAGAAGTTTAAGGAAGGTGTAGAGTTTCTTAGAGACGGTTGGGAAATTGTTAAATTTATCTCAACCTGTGCTTGTGAAATTGTCGGTGGACAAATTGTCACCTGTGCAAAGGAAATTAAGGAGAGTGTTCAGACATTCTTTAAGCTTGTAAATAAATTTTTGGCTTTG'

def test_del_3_as_array():
    text = "Hi there how are you"
    solution = np.array(['H', 'i', ' ', 't', 'h', 'e', 'r', 'e', ' ', 'h', 'o', 'w', ' ', 'a', 'r', 'e', ' ', 'y', 'o', 'u'])
    assert np.all(M.seq_as_array(text) == solution)

def test_del_3_as_arrays():
    texts = ['Hi', 'there', 'how', 'are', 'you']
    solutions = map(np.array,[['H', 'i'], ['t', 'h', 'e', 'r', 'e'], ['h', 'o', 'w'], ['a', 'r', 'e'], ['y', 'o', 'u']])
    checks = [ np.all(M.seq_as_array(t) == s for t,s in zip(texts,solutions)) ]
    assert all(checks)

def test_del_3_align():
    big = np.zeros((3,26),dtype='U1')
    seq_list = map(np.array, [ ['h', 'e', 'l', 'l', 'o', 'b', 'l', 'a', 'h', 'b', 'l', 'u', 'b', 'b', 'a', 'b', 'c', 'd', 'e', 'f', 'f', 'f', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 't', 'a', 'i', 'l'],
                 ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'k', 'm', 'n', 'q', 'q', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 's', 'o', 'm', 'e', 'm', 'o', 'r', 'e', 'h', 'e', 'r', 'e'],
                 ['s', 'o', 'm', 's', 't', 'a', 'r', 't', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
    ])
    start = np.array(['a','b','c'])
    end = np.array(['x','y','z'])
    M.align_all(big, seq_list, start, end)

    result = np.array([
        ['a', 'b', 'c', 'd', 'e', 'f', 'f', 'f', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'k', 'm', 'n', 'q', 'q', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
    ])
    assert np.all(big == result)

def test_del_3_dist_1():
    result = np.array([
        ['a', 'b', 'c', 'd', 'e', 'f', 'f', 'f', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'k', 'm', 'n', 'q', 'q', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
    ])
    assert M.mutation_distance(result[0], result[1]) == 5

def test_del_3_dist_2():
    result = np.array([
        ['a', 'b', 'c', 'd', 'e', 'f', 'f', 'f', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'k', 'm', 'n', 'q', 'q', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
    ])
    assert M.mutation_distance(result[1], result[2]) == 3

def test_del_3_dist_3():
    result = np.array([
        ['a', 'b', 'c', 'd', 'e', 'f', 'f', 'f', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'k', 'm', 'n', 'q', 'q', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
        ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
    ])
    assert M.mutation_distance(result[2], result[0]) == 2
