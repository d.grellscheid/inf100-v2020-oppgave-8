# Include folder above in python path
# Helps travis CI runner find the src files when running py.test
import pytest
#import inspect
import numpy as np

import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

## Tests go below here
import del_1 as M

# func_name, params, expected value
# configs = [
#     (M.a, (3,), [0]*3),
#     (M.a, (7,), [0]*7),
#     (M.a, (1,), [0]*1),

#     (M.b, (5,3), [0,0,0,1,0]),
#     (M.b, (8,0), [1,0,0,0,0,0,0,0]),
#     (M.b, (3,2), [0,0,1]),

#     (M.c, (50,99), range(50,100)),
#     (M.c, (0,49), range(0,50)),
#     (M.c, (20,69), range(20,70)),

#     (M.d, (np.linspace(5,6,11),), np.linspace(6,5,11)),
#     (M.d, (np.linspace(-12,14,27),), np.linspace(14,-12,27)),
#     (M.d, (np.linspace(10,4,7),), np.linspace(4,10,7)),

#     (M.e, (3,3), [[1,1,1],[1,0,1],[1,1,1]]),
#     (M.e, (2,5), [[1,1,1,1,1],[1,1,1,1,1]]),
#     (M.e, (5,5), [[1,1,1,1,1],[1,0,0,0,1],[1,0,0,0,1],[1,0,0,0,1],[1,1,1,1,1]]),

#     (M.f, (3,3,4), [[[1,1,1,1],[1,1,1,1],[1,1,1,1]],[[1,1,1,1],[1,0,0,1],[1,1,1,1]],[[1,1,1,1],[1,1,1,1],[1,1,1,1]]]),
#     (M.f, (2,3,4), [[[1,1,1,1],[1,1,1,1],[1,1,1,1]],[[1,1,1,1],[1,1,1,1],[1,1,1,1]]]),

#     (M.g, (np.array([4,9,1,5,12,16,3,19,22]),), [4,9,1,5,-12,-16,3,-19,22]),
#     (M.g, (np.array([4,9,1,5]),), [4,9,1,5]),
#     (M.g, (np.array([17]),), [-17]),
# ]


# ids = list(('a' * 3) + ('b' * 3) + ('c' * 3) + ('d' * 3) + ('e' * 3) + ('f' * 2) + ('g' * 3))
# @pytest.mark.parametrize('config', configs, ids=ids)
# def test_all(config):
#     fn, param, result = config
#     assert np.all(fn(*param) == np.asarray(result))

def test_del_1_a():
    assert np.all(M.a(3) == np.asarray([0]*3))
    assert np.all(M.a(7) == np.asarray([0]*7))
    assert np.all(M.a(1) == np.asarray([0]*1))

def test_del_1_b():
    assert np.all((M.b(5,3) == [0,0,0,1,0]))
    assert np.all((M.b(8,0) == [1,0,0,0,0,0,0,0]))
    assert np.all((M.b(3,2) == [0,0,1]))

def test_del_1_c():
    assert np.all((M.c(50,99) == range(50,100)))
    assert np.all((M.c(0,49) == range(0,50)))
    assert np.all((M.c(20,69) == range(20,70)))

def test_del_1_d():
    assert np.all((M.d(np.linspace(5,6,11)) == np.linspace(6,5,11)))
    assert np.all((M.d(np.linspace(-12,14,27),) == np.linspace(14,-12,27)))
    assert np.all((M.d(np.linspace(10,4,7)) ==np.linspace(4,10,7)))

def test_del_1_e():
    assert np.all((M.e(3,3) == [[1,1,1],[1,0,1],[1,1,1]]))
    assert np.all((M.e(2,5) == [[1,1,1,1,1],[1,1,1,1,1]]))
    assert np.all((M.e(5,5) == [[1,1,1,1,1],[1,0,0,0,1],[1,0,0,0,1],[1,0,0,0,1],[1,1,1,1,1]]))

def test_del_1_f():
    assert np.all((M.f(3,3,4) == [[[1,1,1,1],[1,1,1,1],[1,1,1,1]],[[1,1,1,1],[1,0,0,1],[1,1,1,1]],[[1,1,1,1],[1,1,1,1],[1,1,1,1]]]))
    assert np.all((M.f(2,3,4) == [[[1,1,1,1],[1,1,1,1],[1,1,1,1]],[[1,1,1,1],[1,1,1,1],[1,1,1,1]]]))

def test_del_1_g():
    assert np.all(M.g(np.array([4,9,1,5,12,16,3,19,22])) == [4,9,1,5,-12,-16,3,-19,22])
    assert np.all(M.g(np.array([4,9,1,5])) == [4,9,1,5])
    assert np.all(M.g(np.array([17])) == [-17])
