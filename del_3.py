import numpy as np

########################
# Read all code first !
########################

def main():
    ############################
    # don't change main!       #
    # you can add print() to   #
    #   see what's happening   #
    ############################

    # load data from file
    titles, seqs = load_data('sequences.fasta')

    # now we have 86 titles and 86 sequences
    if len(titles) == len(seqs) == 86:
        print('Lengths are OK!')

    # convert sequence strings "ATC..." into numpy arrays ['A','T','C',...]
    seqs = seqs_as_arrays(seqs)

    # The seqs have different lengths, around 30000 characters each.
    # They can differ a lot around the first and last few hundred positions, but agree
    # in the middle of the sequence

    # to make sure the sequences can be compared directly, we want to find the middle
    # part where they all agree. The middle is 29655 characters long, and
    # starts and ends with these fragments:
    start_part = seq_as_array('TCGTCTATCTTC')
    end_part = seq_as_array('ATTAATTTTAGT')

    # create one large empty numpy array 86x29655 to hold them all:
    # dtype 'U1' means "one character"
    # 86 lines, of 29655 characters each, with zeros, to be filled next
    all_data = np.zeros((86, 29655),dtype='U1')

    # now we fill the middle parts of all sequences into all_data
    align_all(all_data, seqs, start_part, end_part)

    # they all should start and end the same now
    if np.all(all_data[:,:12] == start_part) and np.all(all_data[:,-12:] == end_part):
        print('Shape OK')

    # in the middle they will disagree in a handful of positions
    # the number of positions where they disagree, we will call mutation_distance

    # check distance between item 0 and 4, should be 5
    if mutation_distance(all_data[0],all_data[4]) == 5:
        print('Distance look OK')

    # calculate distance matrix, of all pairwise distances.
    # this function is already given
    mat = distance_matrix(all_data)

    # this should agree, as a final check
    from matrix_solution import SOLUTION
    if np.all(mat == SOLUTION):
        print('OK! The distance matrix works')

    # and we visualize the distance matrix
    distance_plot(mat)

    # done :)


#############################################
# fyll ut de følgende funksjonene, husk del2
#############################################


def load_data(filename):
    """
    Load fasta sequence file, similar to del_2

    Return a list of labels and a list of sequences.
    The sequence lines for each label should be joined without extra space inbetween.
    """
    labels = []
    seqs = []
    # din kode her
    return labels, seqs




def seq_as_array(seq_str):
    """
    Convert a sequence string into a numpy.array of single letters
    np.array(['A','C','A',...])
    """
    # din kode her
    return np.array([])


def seqs_as_arrays(seqs):
    """Apply seq_as_array to each sequence in a list. Return a list of np.arrays"""
    # din kode her
    return [np.array([]), np.array([]), np.array([]), np.array([]) ]


def align_all(big_array, seq_list, startseq, endseq):
    """
    Fill big_array with the sequences from seq_list.

    The sequences in seq_list have different amounts of material at the beginning and end.
    We only want to save the middle part where they all mostly agree.

    We align them so that they all start with the common sequence 'startseq' and finish with 'endseq'.
    """
    for i, seq in enumerate(seq_list):
        # din kode her,
        # use find_subseq to find the positions of startseq and endseq inside seq
        # fill big_array[i] with the seq between startpos to endpos
        big_array[i] = ''
    # now all lines in big_array start with the same characters and end with the same characters


def mutation_distance(seqA, seqB):
    """Return the number of differences between the numpy arrays seqA and seqB"""
    # din kode her
    return 0










##################################
# ikke endre følgende funksjoner #
##################################

def distance_matrix(data):
    """
    Uses the mutation distance function to do a pairwise distance calculation
    across the data array. Result will be a 86x86 2d-array
    """
    # ikke endre funksjonen
    from scipy.spatial.distance import pdist, squareform
    return squareform(pdist(data, metric=mutation_distance))

def find_subseq(data, seq):
    """
    Return the first index where the seq array is found in the data array.
    """
    # ikke endre funksjonen
    return data.tostring().index(seq.tostring())//data.itemsize

def distance_plot(matrix):
    """
    Visualisering av 2d-array matrix
    """
    # ikke endre funksjonen
    import matplotlib.pyplot as plt
    plt.imshow(matrix, aspect='equal', interpolation='none', origin='lower')
    plt.title('Number of mutations between 2 sequences')
    plt.xlabel('Sequence id')
    plt.ylabel('Sequence id')
    plt.colorbar()
    plt.show()

if __name__ == "__main__":
    main()
